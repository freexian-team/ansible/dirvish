Dirvish
=======

Configure the dirvish backup software.

Role Variables
--------------

`dirvish_banks` is a list of "banks" used to store the backups, the first
item is the default bank.

`dirvish_vaults` is a list of all the vaults managed by dirvish. See the
vault definition below to know what keys you can set in each dictionary.

`dirvish_conf` can contain values to override the default master.conf
file. When you override it, you have to set at least the entries shown
in the example below.

Vault Definition
----------------

* `name` is the name of the vault
* `branch` is the name of the branch (defaults to 'default' if missing)
* `bank` is the path to the bank storing the vault (defaults to the first
   bank listed in `dirvish_banks`)

Example Playbook
----------------

    - name: Configure dirvish backup
      hosts: backup-server
      roles:
        - role: dirvish
	  dirvish_conf:
	    expire-default: +15 days
	    expire-rule:
	      - '#       MIN HR    DOM MON       DOW  STRFTIME_FMT'
	      - '        *   *     *   *         1    +3 months'
	      - '        *   *     1-7 *         1    +1 year'
	    image-time: '22:00'
	    image-default: '%Y%m%d'
	    xdev: 1
	    index: gzip
	  dirvish_vaults:
	    - name: foobar-srv
	      client: foobar.domain.com
	      tree: /srv
	    - name: foobar-etc
	      client: foobar.domain.com
	      tree: /etc
	    - name: baz-stuff
	      client: baz.domain.com
	      tree: /stuff
	      enabled: False

License
-------

BSD

Author Information
------------------

Raphaël Hertzog <raphael@freexian.com>
