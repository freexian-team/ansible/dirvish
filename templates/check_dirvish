#!/bin/sh

# This check script only works well for daily backups
# and backup directories named after the date-based format "%Y%m%d"

date=$(date +%Y%m%d -d "30 hours ago")
date2=$(date +%Y%m%d -d "54 hours ago")
good=0
bad=0

backup_is_good() {
	dir=$1
	date=$2
	if grep -q "^Status: success" $dir/$date/summary 2>/dev/null || \
	   grep -q "^Status: warning (24) -- file vanished on sender" $dir/$date/summary 2>/dev/null; then
		return 0
	fi
	return 1
}

for dir in {% for bank in dirvish_banks %}{{ bank }}/* {% endfor %}; do
	if [ ! -e "$dir" ]; then
		continue
	fi

	basedir=$(basename $dir)
{% if dirvish_check_ignore_vaults %}
	case $basedir in
	    {{ dirvish_check_ignore_vaults|join('|') }})
		# Ignore old backups
		continue
		;;
	esac
{% endif %}

	is_laptop=false
{% if dirvish_check_laptop_vaults %}
	case $basedir in
	    {{ dirvish_check_laptop_vaults|join('|') }})
		is_laptop=true
		;;
	esac
{% endif %}

	if backup_is_good $dir $date; then
		success=${success:+$success }$basedir
		good=$(( $good + 1 ))
	elif [ "$is_laptop" = "true" ] && backup_is_good $dir $date2; then
		success=${success:+$success }$basedir
		good=$(( $good + 1 ))
	else
		failure=${failure:+$failure }$basedir
		bad=$(( $bad + 1 ))
	fi
done

if [ $bad -gt 0 ]; then
	echo "CRITICAL - $bad backup(s) failed ($failure), $good up-to-date ($success)"
	exit 2
else
	echo "GOOD - $good backup(s) up-to-date ($success)"
	exit 0
fi
